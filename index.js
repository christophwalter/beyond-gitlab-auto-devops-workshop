'use strict';

const express = require('express')
const app = express()
const port = 80

app.set('view engine', 'ejs');

app.get("/", (req, res) => {
    res.render(__dirname + "/index.ejs")
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
