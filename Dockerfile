FROM node:16
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm ci

COPY index.ejs index.js ./

EXPOSE 80
CMD [ "node", "index.js" ]
